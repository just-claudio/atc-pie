# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'positionDrawingDialog.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_positionDrawingDialog(object):
    def setupUi(self, positionDrawingDialog):
        positionDrawingDialog.setObjectName("positionDrawingDialog")
        positionDrawingDialog.resize(403, 346)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(positionDrawingDialog)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label = QtWidgets.QLabel(positionDrawingDialog)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.verticalLayout_4.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.reduceHeight_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.reduceHeight_button.setAutoRepeat(True)
        self.reduceHeight_button.setObjectName("reduceHeight_button")
        self.horizontalLayout_2.addWidget(self.reduceHeight_button)
        self.moveDown_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.moveDown_button.setAutoRepeat(True)
        self.moveDown_button.setArrowType(QtCore.Qt.DownArrow)
        self.moveDown_button.setObjectName("moveDown_button")
        self.horizontalLayout_2.addWidget(self.moveDown_button)
        self.increaseHeight_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.increaseHeight_button.setAutoRepeat(True)
        self.increaseHeight_button.setObjectName("increaseHeight_button")
        self.horizontalLayout_2.addWidget(self.increaseHeight_button)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 1, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.moveLeft_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.moveLeft_button.setAutoRepeat(True)
        self.moveLeft_button.setArrowType(QtCore.Qt.LeftArrow)
        self.moveLeft_button.setObjectName("moveLeft_button")
        self.verticalLayout_3.addWidget(self.moveLeft_button)
        self.gridLayout.addLayout(self.verticalLayout_3, 1, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.moveUp_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.moveUp_button.setAutoRepeat(True)
        self.moveUp_button.setArrowType(QtCore.Qt.UpArrow)
        self.moveUp_button.setObjectName("moveUp_button")
        self.horizontalLayout.addWidget(self.moveUp_button)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 1, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.reduceWidth_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.reduceWidth_button.setAutoRepeat(True)
        self.reduceWidth_button.setObjectName("reduceWidth_button")
        self.verticalLayout.addWidget(self.reduceWidth_button)
        self.moveRight_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.moveRight_button.setAutoRepeat(True)
        self.moveRight_button.setArrowType(QtCore.Qt.RightArrow)
        self.moveRight_button.setObjectName("moveRight_button")
        self.verticalLayout.addWidget(self.moveRight_button)
        self.increaseWidth_button = QtWidgets.QToolButton(positionDrawingDialog)
        self.increaseWidth_button.setAutoRepeat(True)
        self.increaseWidth_button.setObjectName("increaseWidth_button")
        self.verticalLayout.addWidget(self.increaseWidth_button)
        self.gridLayout.addLayout(self.verticalLayout, 1, 2, 1, 1)
        self.central_text_area = QtWidgets.QPlainTextEdit(positionDrawingDialog)
        self.central_text_area.setReadOnly(True)
        self.central_text_area.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.central_text_area.setObjectName("central_text_area")
        self.gridLayout.addWidget(self.central_text_area, 1, 1, 1, 1)
        self.verticalLayout_4.addLayout(self.gridLayout)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem1)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem2)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_2 = QtWidgets.QLabel(positionDrawingDialog)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_4.addWidget(self.label_2)
        self.tuningStep_edit = QtWidgets.QSpinBox(positionDrawingDialog)
        self.tuningStep_edit.setMaximum(10000)
        self.tuningStep_edit.setSingleStep(100)
        self.tuningStep_edit.setObjectName("tuningStep_edit")
        self.horizontalLayout_4.addWidget(self.tuningStep_edit)
        self.stepNM_info = QtWidgets.QLabel(positionDrawingDialog)
        self.stepNM_info.setObjectName("stepNM_info")
        self.horizontalLayout_4.addWidget(self.stepNM_info)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.close_button = QtWidgets.QPushButton(positionDrawingDialog)
        self.close_button.setObjectName("close_button")
        self.verticalLayout_4.addWidget(self.close_button)
        self.label_2.setBuddy(self.tuningStep_edit)

        self.retranslateUi(positionDrawingDialog)
        self.close_button.clicked.connect(positionDrawingDialog.accept) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(positionDrawingDialog)
        positionDrawingDialog.setTabOrder(self.central_text_area, self.moveUp_button)
        positionDrawingDialog.setTabOrder(self.moveUp_button, self.moveRight_button)
        positionDrawingDialog.setTabOrder(self.moveRight_button, self.moveDown_button)
        positionDrawingDialog.setTabOrder(self.moveDown_button, self.moveLeft_button)
        positionDrawingDialog.setTabOrder(self.moveLeft_button, self.reduceWidth_button)
        positionDrawingDialog.setTabOrder(self.reduceWidth_button, self.increaseWidth_button)
        positionDrawingDialog.setTabOrder(self.increaseWidth_button, self.reduceHeight_button)
        positionDrawingDialog.setTabOrder(self.reduceHeight_button, self.increaseHeight_button)
        positionDrawingDialog.setTabOrder(self.increaseHeight_button, self.tuningStep_edit)
        positionDrawingDialog.setTabOrder(self.tuningStep_edit, self.close_button)

    def retranslateUi(self, positionDrawingDialog):
        _translate = QtCore.QCoreApplication.translate
        positionDrawingDialog.setWindowTitle(_translate("positionDrawingDialog", "Image positioning helper"))
        self.label.setText(_translate("positionDrawingDialog", "Move/resize the visible images and adjust your .lst file with the new corner coordinates. A file will also be generated in the \"OUTPUT\" directory."))
        self.reduceHeight_button.setText(_translate("positionDrawingDialog", "-h"))
        self.moveDown_button.setText(_translate("positionDrawingDialog", "move\n"
"v"))
        self.increaseHeight_button.setText(_translate("positionDrawingDialog", "+h"))
        self.moveLeft_button.setText(_translate("positionDrawingDialog", "move\n"
"<"))
        self.moveUp_button.setText(_translate("positionDrawingDialog", "move\n"
"^"))
        self.reduceWidth_button.setText(_translate("positionDrawingDialog", "-w"))
        self.moveRight_button.setText(_translate("positionDrawingDialog", "move\n"
">"))
        self.increaseWidth_button.setText(_translate("positionDrawingDialog", "+w"))
        self.label_2.setText(_translate("positionDrawingDialog", "Tuning step:"))
        self.tuningStep_edit.setSuffix(_translate("positionDrawingDialog", " m"))
        self.stepNM_info.setText(_translate("positionDrawingDialog", "###"))
        self.close_button.setText(_translate("positionDrawingDialog", "Close"))
