# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stripDetailsDialog.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_stripDetailsDialog(object):
    def setupUi(self, stripDetailsDialog):
        stripDetailsDialog.setObjectName("stripDetailsDialog")
        stripDetailsDialog.resize(725, 404)
        self.gridLayout_3 = QtWidgets.QGridLayout(stripDetailsDialog)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.assignments_box = QtWidgets.QGroupBox(stripDetailsDialog)
        self.assignments_box.setEnabled(True)
        self.assignments_box.setObjectName("assignments_box")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.assignments_box)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.assignHeading = QtWidgets.QCheckBox(self.assignments_box)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.assignHeading.sizePolicy().hasHeightForWidth())
        self.assignHeading.setSizePolicy(sizePolicy)
        self.assignHeading.setObjectName("assignHeading")
        self.gridLayout_2.addWidget(self.assignHeading, 0, 0, 1, 1)
        self.assignedSpeed_edit = SpeedEditWidget(self.assignments_box)
        self.assignedSpeed_edit.setEnabled(False)
        self.assignedSpeed_edit.setObjectName("assignedSpeed_edit")
        self.gridLayout_2.addWidget(self.assignedSpeed_edit, 2, 1, 1, 1)
        self.assignSpeed = QtWidgets.QCheckBox(self.assignments_box)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.assignSpeed.sizePolicy().hasHeightForWidth())
        self.assignSpeed.setSizePolicy(sizePolicy)
        self.assignSpeed.setObjectName("assignSpeed")
        self.gridLayout_2.addWidget(self.assignSpeed, 2, 0, 1, 1)
        self.assignedHeading_edit = HeadingEditWidget(self.assignments_box)
        self.assignedHeading_edit.setEnabled(False)
        self.assignedHeading_edit.setObjectName("assignedHeading_edit")
        self.gridLayout_2.addWidget(self.assignedHeading_edit, 0, 1, 1, 1)
        self.assignCruiseAlt_button = QtWidgets.QToolButton(self.assignments_box)
        self.assignCruiseAlt_button.setEnabled(False)
        self.assignCruiseAlt_button.setObjectName("assignCruiseAlt_button")
        self.gridLayout_2.addWidget(self.assignCruiseAlt_button, 1, 2, 1, 1)
        self.assignedAltitude_edit = AltFlEditWidget(self.assignments_box)
        self.assignedAltitude_edit.setEnabled(False)
        self.assignedAltitude_edit.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.assignedAltitude_edit.setObjectName("assignedAltitude_edit")
        self.gridLayout_2.addWidget(self.assignedAltitude_edit, 1, 1, 1, 1)
        self.assignAltitude = QtWidgets.QCheckBox(self.assignments_box)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.assignAltitude.sizePolicy().hasHeightForWidth())
        self.assignAltitude.setSizePolicy(sizePolicy)
        self.assignAltitude.setObjectName("assignAltitude")
        self.gridLayout_2.addWidget(self.assignAltitude, 1, 0, 1, 1)
        self.gridLayout_3.addWidget(self.assignments_box, 2, 1, 1, 1)
        self.assignSquawkCode = QtWidgets.QGroupBox(stripDetailsDialog)
        self.assignSquawkCode.setCheckable(True)
        self.assignSquawkCode.setChecked(False)
        self.assignSquawkCode.setObjectName("assignSquawkCode")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.assignSquawkCode)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.xpdrCode_select = XpdrCodeSelectorWidget(self.assignSquawkCode)
        self.xpdrCode_select.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.xpdrCode_select.setObjectName("xpdrCode_select")
        self.verticalLayout_3.addWidget(self.xpdrCode_select)
        self.gridLayout_3.addWidget(self.assignSquawkCode, 1, 1, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.linkFpl_widget = QtWidgets.QWidget(stripDetailsDialog)
        self.linkFpl_widget.setObjectName("linkFpl_widget")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.linkFpl_widget)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_4 = QtWidgets.QLabel(self.linkFpl_widget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_4.addWidget(self.label_4)
        self.linkFpl_select = QtWidgets.QComboBox(self.linkFpl_widget)
        self.linkFpl_select.setObjectName("linkFpl_select")
        self.horizontalLayout_4.addWidget(self.linkFpl_select)
        self.linkFpl_matching_info = QtWidgets.QLabel(self.linkFpl_widget)
        self.linkFpl_matching_info.setObjectName("linkFpl_matching_info")
        self.horizontalLayout_4.addWidget(self.linkFpl_matching_info)
        self.horizontalLayout_3.addWidget(self.linkFpl_widget)
        self.linkedFpl_widget = QtWidgets.QWidget(stripDetailsDialog)
        self.linkedFpl_widget.setObjectName("linkedFpl_widget")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.linkedFpl_widget)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.linkedFpl_label = QtWidgets.QLabel(self.linkedFpl_widget)
        self.linkedFpl_label.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.linkedFpl_label.setObjectName("linkedFpl_label")
        self.horizontalLayout_6.addWidget(self.linkedFpl_label)
        self.linkedFplStatus_info = QtWidgets.QLabel(self.linkedFpl_widget)
        self.linkedFplStatus_info.setObjectName("linkedFplStatus_info")
        self.horizontalLayout_6.addWidget(self.linkedFplStatus_info)
        self.horizontalLayout_3.addWidget(self.linkedFpl_widget)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.gridLayout_3.addLayout(self.horizontalLayout_3, 6, 0, 1, 1)
        self.line = QtWidgets.QFrame(stripDetailsDialog)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout_3.addWidget(self.line, 5, 0, 1, 2)
        self.buttonBox = QtWidgets.QDialogButtonBox(stripDetailsDialog)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_3.addWidget(self.buttonBox, 6, 1, 1, 1)
        self.rack_groupBox = QtWidgets.QGroupBox(stripDetailsDialog)
        self.rack_groupBox.setObjectName("rack_groupBox")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.rack_groupBox)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.rack_select = QtWidgets.QComboBox(self.rack_groupBox)
        self.rack_select.setObjectName("rack_select")
        self.verticalLayout_4.addWidget(self.rack_select)
        self.gridLayout_3.addWidget(self.rack_groupBox, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem1, 3, 1, 1, 1)
        self.widget = QtWidgets.QWidget(stripDetailsDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.depClearance_widget = QtWidgets.QWidget(self.widget)
        self.depClearance_widget.setObjectName("depClearance_widget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.depClearance_widget)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.depClearance_label = QtWidgets.QLabel(self.depClearance_widget)
        self.depClearance_label.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.depClearance_label.setObjectName("depClearance_label")
        self.horizontalLayout_2.addWidget(self.depClearance_label)
        self.viewDepClearance_button = QtWidgets.QToolButton(self.depClearance_widget)
        self.viewDepClearance_button.setAutoRaise(True)
        self.viewDepClearance_button.setObjectName("viewDepClearance_button")
        self.horizontalLayout_2.addWidget(self.viewDepClearance_button)
        self.horizontalLayout.addWidget(self.depClearance_widget)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.gridLayout_3.addWidget(self.widget, 4, 1, 1, 1)
        self.stripInfo_box = QtWidgets.QGroupBox(stripDetailsDialog)
        self.stripInfo_box.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.stripInfo_box.sizePolicy().hasHeightForWidth())
        self.stripInfo_box.setSizePolicy(sizePolicy)
        self.stripInfo_box.setObjectName("stripInfo_box")
        self.gridLayout = QtWidgets.QGridLayout(self.stripInfo_box)
        self.gridLayout.setObjectName("gridLayout")
        self.flightRules_select = QtWidgets.QComboBox(self.stripInfo_box)
        self.flightRules_select.setObjectName("flightRules_select")
        self.flightRules_select.addItem("")
        self.flightRules_select.setItemText(0, "")
        self.flightRules_select.addItem("")
        self.flightRules_select.addItem("")
        self.gridLayout.addWidget(self.flightRules_select, 0, 5, 1, 2)
        self.arrAirportName_info = QtWidgets.QLabel(self.stripInfo_box)
        font = QtGui.QFont()
        font.setItalic(True)
        self.arrAirportName_info.setFont(font)
        self.arrAirportName_info.setObjectName("arrAirportName_info")
        self.gridLayout.addWidget(self.arrAirportName_info, 3, 4, 1, 3)
        self.arrAirportPicker_widget = AirportPicker(self.stripInfo_box)
        self.arrAirportPicker_widget.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.arrAirportPicker_widget.setObjectName("arrAirportPicker_widget")
        self.gridLayout.addWidget(self.arrAirportPicker_widget, 3, 1, 1, 2)
        self.label_3 = QtWidgets.QLabel(self.stripInfo_box)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 3, 0, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.stripInfo_box)
        self.label_14.setObjectName("label_14")
        self.gridLayout.addWidget(self.label_14, 4, 0, 1, 1)
        self.comments_edit = QtWidgets.QPlainTextEdit(self.stripInfo_box)
        self.comments_edit.setTabChangesFocus(True)
        self.comments_edit.setPlainText("")
        self.comments_edit.setObjectName("comments_edit")
        self.gridLayout.addWidget(self.comments_edit, 7, 1, 1, 6)
        self.line_3 = QtWidgets.QFrame(self.stripInfo_box)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout.addWidget(self.line_3, 6, 0, 1, 7)
        self.label_5 = QtWidgets.QLabel(self.stripInfo_box)
        self.label_5.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 0, 0, 1, 1)
        self.depAirportName_info = QtWidgets.QLabel(self.stripInfo_box)
        font = QtGui.QFont()
        font.setItalic(True)
        self.depAirportName_info.setFont(font)
        self.depAirportName_info.setObjectName("depAirportName_info")
        self.gridLayout.addWidget(self.depAirportName_info, 2, 4, 1, 3)
        spacerItem4 = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem4, 0, 3, 1, 1)
        self.label = QtWidgets.QLabel(self.stripInfo_box)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.stripInfo_box)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 1, 4, 1, 1)
        self.cruiseAlt_edit = AltFlEditWidget(self.stripInfo_box)
        self.cruiseAlt_edit.setEnabled(False)
        self.cruiseAlt_edit.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.cruiseAlt_edit.setObjectName("cruiseAlt_edit")
        self.gridLayout.addWidget(self.cruiseAlt_edit, 5, 1, 1, 2)
        self.duplicateCallsign_label = QtWidgets.QLabel(self.stripInfo_box)
        self.duplicateCallsign_label.setObjectName("duplicateCallsign_label")
        self.gridLayout.addWidget(self.duplicateCallsign_label, 0, 2, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.stripInfo_box)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 0, 4, 1, 1)
        self.cruiseAlt_enable = QtWidgets.QCheckBox(self.stripInfo_box)
        self.cruiseAlt_enable.setObjectName("cruiseAlt_enable")
        self.gridLayout.addWidget(self.cruiseAlt_enable, 5, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.stripInfo_box)
        self.label_2.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.aircraftType_edit = AircraftTypeCombo(self.stripInfo_box)
        self.aircraftType_edit.setEditable(True)
        self.aircraftType_edit.setObjectName("aircraftType_edit")
        self.gridLayout.addWidget(self.aircraftType_edit, 1, 1, 1, 1)
        self.callsign_edit = QtWidgets.QLineEdit(self.stripInfo_box)
        self.callsign_edit.setClearButtonEnabled(True)
        self.callsign_edit.setObjectName("callsign_edit")
        self.gridLayout.addWidget(self.callsign_edit, 0, 1, 1, 1)
        self.depAirportPicker_widget = AirportPicker(self.stripInfo_box)
        self.depAirportPicker_widget.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.depAirportPicker_widget.setObjectName("depAirportPicker_widget")
        self.gridLayout.addWidget(self.depAirportPicker_widget, 2, 1, 1, 2)
        self.autoFillWTC_button = QtWidgets.QToolButton(self.stripInfo_box)
        self.autoFillWTC_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.autoFillWTC_button.setCheckable(True)
        self.autoFillWTC_button.setChecked(True)
        self.autoFillWTC_button.setArrowType(QtCore.Qt.RightArrow)
        self.autoFillWTC_button.setObjectName("autoFillWTC_button")
        self.gridLayout.addWidget(self.autoFillWTC_button, 1, 2, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.stripInfo_box)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy)
        self.label_8.setObjectName("label_8")
        self.gridLayout.addWidget(self.label_8, 7, 0, 1, 1)
        self.route_edit = RouteEditWidget(self.stripInfo_box)
        self.route_edit.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.route_edit.setObjectName("route_edit")
        self.gridLayout.addWidget(self.route_edit, 4, 1, 1, 6)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.TAS_enable = QtWidgets.QCheckBox(self.stripInfo_box)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.TAS_enable.sizePolicy().hasHeightForWidth())
        self.TAS_enable.setSizePolicy(sizePolicy)
        self.TAS_enable.setObjectName("TAS_enable")
        self.horizontalLayout_5.addWidget(self.TAS_enable)
        self.TAS_edit = SpeedEditWidget(self.stripInfo_box)
        self.TAS_edit.setEnabled(False)
        self.TAS_edit.setObjectName("TAS_edit")
        self.horizontalLayout_5.addWidget(self.TAS_edit)
        self.gridLayout.addLayout(self.horizontalLayout_5, 5, 4, 1, 3)
        self.wakeTurbCat_select = QtWidgets.QComboBox(self.stripInfo_box)
        self.wakeTurbCat_select.setObjectName("wakeTurbCat_select")
        self.wakeTurbCat_select.addItem("")
        self.wakeTurbCat_select.setItemText(0, "")
        self.wakeTurbCat_select.addItem("")
        self.wakeTurbCat_select.addItem("")
        self.wakeTurbCat_select.addItem("")
        self.wakeTurbCat_select.addItem("")
        self.gridLayout.addWidget(self.wakeTurbCat_select, 1, 5, 1, 2)
        self.label_5.raise_()
        self.callsign_edit.raise_()
        self.label_2.raise_()
        self.aircraftType_edit.raise_()
        self.label_8.raise_()
        self.comments_edit.raise_()
        self.line_3.raise_()
        self.label.raise_()
        self.label_3.raise_()
        self.depAirportName_info.raise_()
        self.arrAirportName_info.raise_()
        self.depAirportPicker_widget.raise_()
        self.label_14.raise_()
        self.arrAirportPicker_widget.raise_()
        self.route_edit.raise_()
        self.label_9.raise_()
        self.flightRules_select.raise_()
        self.label_10.raise_()
        self.wakeTurbCat_select.raise_()
        self.autoFillWTC_button.raise_()
        self.cruiseAlt_enable.raise_()
        self.cruiseAlt_edit.raise_()
        self.duplicateCallsign_label.raise_()
        self.gridLayout_3.addWidget(self.stripInfo_box, 0, 0, 5, 1)
        self.label_4.setBuddy(self.linkFpl_select)
        self.linkFpl_matching_info.setBuddy(self.linkFpl_select)
        self.depClearance_label.setBuddy(self.viewDepClearance_button)
        self.label_3.setBuddy(self.arrAirportPicker_widget)
        self.label_14.setBuddy(self.route_edit)
        self.label_5.setBuddy(self.callsign_edit)
        self.label.setBuddy(self.depAirportPicker_widget)
        self.label_10.setBuddy(self.wakeTurbCat_select)
        self.label_9.setBuddy(self.flightRules_select)
        self.label_2.setBuddy(self.aircraftType_edit)
        self.label_8.setBuddy(self.comments_edit)

        self.retranslateUi(stripDetailsDialog)
        self.assignHeading.toggled['bool'].connect(self.assignedHeading_edit.setEnabled) # type: ignore
        self.assignSpeed.toggled['bool'].connect(self.assignedSpeed_edit.setEnabled) # type: ignore
        self.TAS_enable.toggled['bool'].connect(self.TAS_edit.setEnabled) # type: ignore
        self.TAS_enable.toggled['bool'].connect(self.TAS_edit.setFocus) # type: ignore
        self.assignHeading.toggled['bool'].connect(self.assignedHeading_edit.setFocus) # type: ignore
        self.assignSpeed.toggled['bool'].connect(self.assignedSpeed_edit.setFocus) # type: ignore
        self.assignSquawkCode.toggled['bool'].connect(self.xpdrCode_select.setFocus) # type: ignore
        self.assignAltitude.toggled['bool'].connect(self.assignedAltitude_edit.setEnabled) # type: ignore
        self.assignAltitude.toggled['bool'].connect(self.assignedAltitude_edit.setFocus) # type: ignore
        self.cruiseAlt_enable.toggled['bool'].connect(self.cruiseAlt_edit.setEnabled) # type: ignore
        self.cruiseAlt_enable.toggled['bool'].connect(self.cruiseAlt_edit.setFocus) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(stripDetailsDialog)
        stripDetailsDialog.setTabOrder(self.callsign_edit, self.aircraftType_edit)
        stripDetailsDialog.setTabOrder(self.aircraftType_edit, self.depAirportPicker_widget)
        stripDetailsDialog.setTabOrder(self.depAirportPicker_widget, self.arrAirportPicker_widget)
        stripDetailsDialog.setTabOrder(self.arrAirportPicker_widget, self.route_edit)
        stripDetailsDialog.setTabOrder(self.route_edit, self.cruiseAlt_enable)
        stripDetailsDialog.setTabOrder(self.cruiseAlt_enable, self.cruiseAlt_edit)
        stripDetailsDialog.setTabOrder(self.cruiseAlt_edit, self.flightRules_select)
        stripDetailsDialog.setTabOrder(self.flightRules_select, self.wakeTurbCat_select)
        stripDetailsDialog.setTabOrder(self.wakeTurbCat_select, self.TAS_enable)
        stripDetailsDialog.setTabOrder(self.TAS_enable, self.TAS_edit)
        stripDetailsDialog.setTabOrder(self.TAS_edit, self.comments_edit)
        stripDetailsDialog.setTabOrder(self.comments_edit, self.rack_select)
        stripDetailsDialog.setTabOrder(self.rack_select, self.assignSquawkCode)
        stripDetailsDialog.setTabOrder(self.assignSquawkCode, self.xpdrCode_select)
        stripDetailsDialog.setTabOrder(self.xpdrCode_select, self.assignHeading)
        stripDetailsDialog.setTabOrder(self.assignHeading, self.assignedHeading_edit)
        stripDetailsDialog.setTabOrder(self.assignedHeading_edit, self.assignAltitude)
        stripDetailsDialog.setTabOrder(self.assignAltitude, self.assignedAltitude_edit)
        stripDetailsDialog.setTabOrder(self.assignedAltitude_edit, self.assignCruiseAlt_button)
        stripDetailsDialog.setTabOrder(self.assignCruiseAlt_button, self.assignSpeed)
        stripDetailsDialog.setTabOrder(self.assignSpeed, self.assignedSpeed_edit)

    def retranslateUi(self, stripDetailsDialog):
        _translate = QtCore.QCoreApplication.translate
        stripDetailsDialog.setWindowTitle(_translate("stripDetailsDialog", "Strip details"))
        self.assignments_box.setTitle(_translate("stripDetailsDialog", "Assignments/vectors"))
        self.assignHeading.setText(_translate("stripDetailsDialog", "Heading:"))
        self.assignSpeed.setText(_translate("stripDetailsDialog", "Speed:"))
        self.assignCruiseAlt_button.setToolTip(_translate("stripDetailsDialog", "Assign cruise altitude/FL"))
        self.assignCruiseAlt_button.setText(_translate("stripDetailsDialog", "⤒"))
        self.assignAltitude.setText(_translate("stripDetailsDialog", "Altitude/FL:"))
        self.assignSquawkCode.setTitle(_translate("stripDetailsDialog", "Transponder code"))
        self.label_4.setText(_translate("stripDetailsDialog", "Link FPL:"))
        self.linkFpl_matching_info.setToolTip(_translate("stripDetailsDialog", "Unlinked flight plans matching callsign filter today"))
        self.linkFpl_matching_info.setText(_translate("stripDetailsDialog", "(##)"))
        self.linkedFpl_label.setToolTip(_translate("stripDetailsDialog", "A flight plan is linked"))
        self.linkedFpl_label.setText(_translate("stripDetailsDialog", "FPL"))
        self.linkedFplStatus_info.setText(_translate("stripDetailsDialog", "###"))
        self.rack_groupBox.setTitle(_translate("stripDetailsDialog", "Rack"))
        self.depClearance_label.setToolTip(_translate("stripDetailsDialog", "A departure clearance is registered"))
        self.depClearance_label.setText(_translate("stripDetailsDialog", "DEP"))
        self.viewDepClearance_button.setText(_translate("stripDetailsDialog", "V"))
        self.stripInfo_box.setTitle(_translate("stripDetailsDialog", "Strip details"))
        self.flightRules_select.setItemText(1, _translate("stripDetailsDialog", "IFR"))
        self.flightRules_select.setItemText(2, _translate("stripDetailsDialog", "VFR"))
        self.arrAirportName_info.setText(_translate("stripDetailsDialog", "##"))
        self.label_3.setText(_translate("stripDetailsDialog", "Destination:"))
        self.label_14.setText(_translate("stripDetailsDialog", "Route:"))
        self.label_5.setText(_translate("stripDetailsDialog", "Callsign:"))
        self.depAirportName_info.setText(_translate("stripDetailsDialog", "##"))
        self.label.setText(_translate("stripDetailsDialog", "Origin:"))
        self.label_10.setText(_translate("stripDetailsDialog", "WTC:"))
        self.duplicateCallsign_label.setText(_translate("stripDetailsDialog", "!!dup"))
        self.label_9.setText(_translate("stripDetailsDialog", "Rules:"))
        self.cruiseAlt_enable.setText(_translate("stripDetailsDialog", "Cruise:"))
        self.label_2.setText(_translate("stripDetailsDialog", "Aircraft type:"))
        self.autoFillWTC_button.setToolTip(_translate("stripDetailsDialog", "WTC auto-fill with type change"))
        self.autoFillWTC_button.setText(_translate("stripDetailsDialog", ">>"))
        self.label_8.setText(_translate("stripDetailsDialog", "Comments:"))
        self.TAS_enable.setText(_translate("stripDetailsDialog", "TAS:"))
        self.wakeTurbCat_select.setItemText(1, _translate("stripDetailsDialog", "L"))
        self.wakeTurbCat_select.setItemText(2, _translate("stripDetailsDialog", "M"))
        self.wakeTurbCat_select.setItemText(3, _translate("stripDetailsDialog", "H"))
        self.wakeTurbCat_select.setItemText(4, _translate("stripDetailsDialog", "J"))
from gui.widgets.adWidgets import AirportPicker
from gui.widgets.basicWidgets import AircraftTypeCombo, AltFlEditWidget, HeadingEditWidget, SpeedEditWidget
from gui.widgets.miscWidgets import XpdrCodeSelectorWidget
from gui.widgets.routeWidgets import RouteEditWidget
