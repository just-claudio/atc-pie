# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'radioTextChat.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_radioTextChatPanel(object):
    def setupUi(self, radioTextChatPanel):
        radioTextChatPanel.setObjectName("radioTextChatPanel")
        radioTextChatPanel.resize(678, 192)
        self.verticalLayout = QtWidgets.QVBoxLayout(radioTextChatPanel)
        self.verticalLayout.setObjectName("verticalLayout")
        self.chatHistory_view = QtWidgets.QTableView(radioTextChatPanel)
        self.chatHistory_view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.chatHistory_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.chatHistory_view.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.chatHistory_view.setShowGrid(False)
        self.chatHistory_view.setObjectName("chatHistory_view")
        self.chatHistory_view.horizontalHeader().setVisible(False)
        self.chatHistory_view.horizontalHeader().setStretchLastSection(True)
        self.chatHistory_view.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.chatHistory_view)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(radioTextChatPanel)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.dest_combo = QtWidgets.QComboBox(radioTextChatPanel)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dest_combo.sizePolicy().hasHeightForWidth())
        self.dest_combo.setSizePolicy(sizePolicy)
        self.dest_combo.setEditable(True)
        self.dest_combo.setInsertPolicy(QtWidgets.QComboBox.NoInsert)
        self.dest_combo.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)
        self.dest_combo.setMinimumContentsLength(10)
        self.dest_combo.setObjectName("dest_combo")
        self.horizontalLayout.addWidget(self.dest_combo)
        self.label_2 = QtWidgets.QLabel(radioTextChatPanel)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.chatLine_input = QtWidgets.QComboBox(radioTextChatPanel)
        self.chatLine_input.setEditable(True)
        self.chatLine_input.setInsertPolicy(QtWidgets.QComboBox.NoInsert)
        self.chatLine_input.setObjectName("chatLine_input")
        self.horizontalLayout.addWidget(self.chatLine_input)
        self.send_button = QtWidgets.QToolButton(radioTextChatPanel)
        self.send_button.setObjectName("send_button")
        self.horizontalLayout.addWidget(self.send_button)
        self.menu_button = QtWidgets.QToolButton(radioTextChatPanel)
        self.menu_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.menu_button.setObjectName("menu_button")
        self.horizontalLayout.addWidget(self.menu_button)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(radioTextChatPanel)
        QtCore.QMetaObject.connectSlotsByName(radioTextChatPanel)
        radioTextChatPanel.setTabOrder(self.chatHistory_view, self.dest_combo)
        radioTextChatPanel.setTabOrder(self.dest_combo, self.chatLine_input)
        radioTextChatPanel.setTabOrder(self.chatLine_input, self.send_button)

    def retranslateUi(self, radioTextChatPanel):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("radioTextChatPanel", "To:"))
        self.label_2.setText(_translate("radioTextChatPanel", "Msg:"))
        self.send_button.setText(_translate("radioTextChatPanel", "Send"))
        self.menu_button.setText(_translate("radioTextChatPanel", "Opts"))

