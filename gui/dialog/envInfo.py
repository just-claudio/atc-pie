
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
#
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from math import atan, degrees

from PyQt5.QtCore import Qt, QAbstractTableModel
from PyQt5.QtWidgets import QDialog

from ui.envInfoDialog import Ui_envInfoDialog

from base.coords import m2NM
from base.strip import rack_detail, runway_box_detail, shelved_detail, sent_to_detail
from base.util import some

from session.env import env

from ext.xplane import surface_types

from gui.misc import RadioKeyEventFilter


# ---------- Constants ----------

# -------------------------------


class RwyInfoTableModel(QAbstractTableModel):
    """
    CAUTION: Do not build if no airport data
    """

    def __init__(self, parent):
        QAbstractTableModel.__init__(self, parent)
        self.column_headers = ['RWY', 'LOC freq.', 'GS', 'Surface', 'Orientation', 'Length', 'Width', 'DTHR', 'THR elev.']
        self.runways = env.airport_data.allRunways(sortByName=True)

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.column_headers[section]

    def rowCount(self, parent=None):
        return len(self.runways)

    def columnCount(self, parent=None):
        return len(self.column_headers)

    def data(self, index, role):
        if role == Qt.DisplayRole:
            rwy = self.runways[index.row()]
            col = index.column()
            width, surface = env.airport_data.physicalRunwayData(rwy.physicalRwyIndex())
            if col == 0:  # RWY
                return rwy.name
            elif col == 1:  # LOC freq.
                txt = some(rwy.LOC_freq, 'none')
                if rwy.ILS_cat is not None:
                    txt += ' (%s)' % rwy.ILS_cat
                return txt
            elif col == 2:  # GS
                if rwy.hasILS():
                    return '%.1f%% / %.1f°' % (rwy.param_FPA, degrees(atan(rwy.param_FPA / 100)))
                else:
                    return 'none'
            elif col == 3:  # Surface
                return surface_types.get(surface, 'Unknown')
            elif col == 4:  # Orientation
                return '%s°' % rwy.orientation().read()
            elif col == 5:  # Length
                return '%d m' % (rwy.length(dthr=False) / m2NM)
            elif col == 6:  # Width
                return '%d m' % width
            elif col == 7:  # DTHR
                return 'none' if rwy.dthr == 0 else '%d m' % rwy.dthr
            elif col == 8:  # THR elev.
                return 'N/A' if env.elevation_map is None else '%.1f ft' % env.elevation(rwy.threshold())



class HelipadInfoTableModel(QAbstractTableModel):
    """
    CAUTION: Do not build if no airport data
    """

    def __init__(self, parent):
        QAbstractTableModel.__init__(self, parent)
        self.column_headers = ['Helipad', 'Surface', 'Width', 'Elev.']
        self.helipads = sorted(env.airport_data.helipads, key=(lambda r: r.name))

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.column_headers[section]

    def rowCount(self, parent=None):
        return len(self.helipads)

    def columnCount(self, parent=None):
        return len(self.column_headers)

    def data(self, index, role):
        if role == Qt.DisplayRole:
            pad = self.helipads[index.row()]
            col = index.column()
            if col == 0:  # Helipad
                return pad.name
            elif col == 1:  # Surface
                return surface_types.get(pad.surface, 'Unknown')
            elif col == 2:  # Width
                return '%d m' % min(pad.width, pad.length)
            elif col == 3:  # Elev.
                return 'N/A' if env.elevation_map is None else '%.1f ft' % env.elevation(pad.centre)


class EnvironmentInfoDialog(QDialog, Ui_envInfoDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.installEventFilter(RadioKeyEventFilter(self))
        self.radarPosition_infoLabel.setText(str(env.radarPos()))
        if env.airport_data is None:
            self.airport_tab.setEnabled(False)
        else:
            self.airportName_infoLabel.setText(env.locationName())
            self.airportElevation_infoLabel.setText('%.1f ft' % env.airport_data.field_elevation)
            rwy_table_model = RwyInfoTableModel(self)
            self.runway_tableView.setModel(rwy_table_model)
            if rwy_table_model.rowCount() == 0:
                self.runway_tableView.setVisible(False)
            else:
                for i in range(rwy_table_model.columnCount()):
                    self.runway_tableView.resizeColumnToContents(i)
            helipad_table_model = HelipadInfoTableModel(self)
            self.helipad_tableView.setModel(helipad_table_model)
            if helipad_table_model.rowCount() == 0:
                self.helipad_tableView.setVisible(False)
            else:
                for i in range(helipad_table_model.columnCount()):
                    self.helipad_tableView.resizeColumnToContents(i)

    def showEvent(self, event):
        self.magneticDeclination_infoLabel.setText(env.readDeclination())
        racked = env.strips.count(lambda s: s.lookup(rack_detail) is not None)
        boxed = env.strips.count(lambda s: s.lookup(runway_box_detail) is not None)
        total = env.strips.count()
        sent = env.discarded_strips.count(lambda s: s.lookup(sent_to_detail) is not None)
        shelved = env.discarded_strips.count(lambda s: s.lookup(shelved_detail))
        self.rackedStrips_infoLabel.setText(str(racked))
        self.looseStrips_infoLabel.setText(str(total - racked - boxed))
        self.boxedStrips_infoLabel.setText(str(boxed))
        self.activeStrips_infoLabel.setText(str(total))
        self.sentStrips_infoLabel.setText(str(sent))
        self.shelvedStrips_infoLabel.setText(str(shelved))
        self.releasedStrips_infoLabel.setText(str(sent + shelved))
        self.xpdrLinkedStrips_infoLabel.setText(str(env.strips.count(lambda s: s.linkedAircraft() is not None)))
        self.fplLinkedStrips_infoLabel.setText(str(env.strips.count(lambda s: s.linkedFPL() is not None)))
